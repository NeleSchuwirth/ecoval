#################################
# create package
#################################


# prepare data:
ecoval.dictionaries.default <- 
  read.csv("./ecoval/inst/ecoval.dictionaries.default.dat",header=T,sep="\t",stringsAsFactors=FALSE)
msk.macrophytes.2017_RiverTypes_DefStruct <- 
  read.csv("./ecoval/inst/msk.macrophytes.2017_RiverTypes_DefStruct.dat",header=T,sep="\t",stringsAsFactors=FALSE)
msk.macrophytes.2017_RiverTypes_DefLimitsUnc <- 
  read.csv("./ecoval/inst/msk.macrophytes.2017_RiverTypes_DefLimitsUnc.dat",header=F,sep="\t",stringsAsFactors=FALSE)
msk.macrophytes.2017_RiverTypes_DefObsUnc <- 
  read.csv("./ecoval/inst/msk.macrophytes.2017_RiverTypes_DefObsUnc.dat",header=F,sep="\t",stringsAsFactors=FALSE)
msk.macrophytes.2017_ListTaxa <- 
  read.csv("./ecoval/inst/msk.macrophytes.2017_ListTaxa.dat",header=T,sep="\t",stringsAsFactors=FALSE,encoding="UTF-8",na=c(NA,""))

save(ecoval.dictionaries.default,
     msk.macrophytes.2017_RiverTypes_DefStruct,
     msk.macrophytes.2017_RiverTypes_DefLimitsUnc,
     msk.macrophytes.2017_RiverTypes_DefObsUnc,
     msk.macrophytes.2017_ListTaxa,
     file="./ecoval/data/ecoval_data.RData")


# generate plots for manual:
source("./ecoval/inst/ecoval_manual_plots.r")

# check package:
system("R CMD check ecoval")

# build source package (.tar.gz):
system("R CMD build ecoval")

# check processed package:
system("R CMD check --as-cran ecoval_1.2.9.tar.gz")

install.packages("ecoval_1.2.9.tar.gz",repos=NULL,type="source")
library(ecoval)
help(ecoval)

# upload:
# https://cran.r-project.org/submit.html
