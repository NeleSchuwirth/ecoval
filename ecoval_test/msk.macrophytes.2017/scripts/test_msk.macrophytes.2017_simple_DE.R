# load package:

if ( !require("ecoval") ) { install.packages("ecoval"); library(ecoval) }

# create output folder:

if ( !dir.exists("../output/DE") ) dir.create("../output/DE",recursive=TRUE)

# perform analysis with results from plausibilization (comment line "file.typeplaus ..." for preliminary results without):

res <- msk.macrophytes.2017.read.compile.evaluate(file.site         = "../input/DE/Standortdaten_max.txt",
                                                  pic.folder        = "../input/pictures",
                                                  file.species      = "../input/DE/Artdaten.txt",
                                                  file.typeplaus    = "../input/DE/Flusstypen_Plaus.txt",
                                                  sampling.protocol = "v2018",
                                                  sampsize          = 10000,
                                                  file.res          = "../output/DE/Standortdaten_Attribute_Bewertung_Final_max.txt",
                                                  file.doc          = "../output/DE/Standort_Dokumentation_max.pdf",
                                                  file.taxa.used    = "../output/DE/Taxa_verwendet.txt",
                                                  file.taxa.removed = "../output/DE/Taxa_entfernt.txt",
                                                  file.check.msg    = "../output/DE/Qualitaets_Check.txt",
                                                  language          = "Deutsch")

# write taxa list (for information only):

write.table(res$taxalist,"../output/DE/Taxa_Liste.txt",col.names=T,row.names=F,sep="\t",na="")
