Code_Sampling	coverage_class	riverbedwidth_m	widthvariability_class	depthvariability_class	bedmodfract_class	bedmodtype_class	bankmodfract_L_class	bankmodperm_L_class	bankmodfract_R_class	bankmodperm_R_class	riparianzonewidth_L_m	riparianzone_vegmat_L_class	riparianzone_veg_L_class	riparianzonewidth_R_m	riparianzone_vegmat_R_class	riparianzone_veg_R_class	surroundings_vegmat_L_class	surroundings_vegmat_R_class
355_26.05.2016	open	6.5	moderate	moderate	0to10	other4	30to60	imperm3	10to30	perm2	5	forest	natural	5	forest	natural	8	4
353_23.06.2016	open	4	none	none	100	riprap2	100	perm2	100	perm2	6	treesshrub	natural	5	treesshrub	natural	4	4
5724_27.06.2016	open	2	moderate	moderate	0	riprap1	0	0	0		6	meadow	seminatural	6	meadow	seminatural	4	4
463_04.07.2016	open	5	moderate	high	0	riprap1	10to30	perm2	10to30	perm2	2	treesshrub	natural	5	treesshrub	natural	8	4
200_16.08.2016	open	8	high	high	0to10	riprap2	0to10	perm2	0to10	perm2	10	meadow	seminatural	10	meadow	seminatural	8	8
200_21.08.2017	open	6	high	high	0to10	riprap2	0to10	perm2	0to10	perm2	6	meadow	seminatural	7	meadow	seminatural	8	8
5640_18.07.2017	open	12	none	moderate	0	riprap2	60to100	imperm2	60to100	imperm2	3	treesshrub	natural	5	treesshrub	natural	8	4
9222_04.08.2017	open	3	none	moderate	0	riprap1	0	0	0		3	meadow	seminatural	3	meadow	seminatural	4	4
5893_27.06.2016	open	4	high	high	0	riprap1	0	0	0		15	forest	natural	15	forest	natural	1	1
9186_17.08.2017	open	6	none	none	100	other3	100	imperm4	100	imperm4	8	forest	natural	8	forest	natural	4	4
